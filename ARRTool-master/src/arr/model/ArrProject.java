package arr.model;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import arr.util.Util;

public class ArrProject {

	private ArrPackage rootPackage;
	private IJavaProject iProject;

	public ArrProject(IJavaProject project) throws JavaModelException {
		this.setjProject(project);
		ArrPackage rootPackage = new ArrPackage();
		rootPackage.setName("root");
		this.rootPackage = rootPackage;
		this.addIPackages(project.getPackageFragments());
		ArrayList<ArrPackage> packages = getPackages();
		for (ArrPackage pkg : packages) {
			pkg.createDependencies();
		}
	}

	/**
	 * Add an array of IPackageFragment.
	 * 
	 * @param iPackages
	 *            an array of IPackageFragment.
	 * @throws JavaModelException
	 */
	private void addIPackages(IPackageFragment[] iPackages) throws JavaModelException {
		for (IPackageFragment iPackage : iPackages) {
			ICompilationUnit[] classFiles = iPackage.getCompilationUnits();
			if (classFiles.length > 0) {
				this.addIPackage(iPackage, iPackages);
			}
		}
	}

	/**
	 * Add an IPackage and its ancestors to the arrPackageTree if it is not
	 * there yet. This method also adds the special package if necessary.
	 * 
	 * @param iPackage
	 *            IPacakge to be added
	 * @param iPackages
	 *            List of all IPackages
	 * @return ArrPackage added
	 * @throws JavaModelException
	 */
	private ArrPackage addIPackage(IPackageFragment iPackage, IPackageFragment[] iPackages) throws JavaModelException {
		String name = iPackage.getElementName();
		ArrPackage newPackage = getPackageByName(name);
		if (newPackage == null) {
			newPackage = new ArrPackage(this, iPackage);
			String parentName = newPackage.getParentName();
			ArrPackage parentPackage = getPackageByName(parentName);
			if (parentPackage == null) {
				IPackageFragment iParentPackage = Util.findIPackageByName(parentName, iPackages);
				parentPackage = addIPackage(iParentPackage, iPackages);
			}
			newPackage.setParent(parentPackage);
			if (newPackage.isSpecialPackage()) {
				newPackage = addSpecialArrPackage(newPackage);
			}
			newPackage.addClasses(iPackage.getCompilationUnits());
		}
		return newPackage;
	}

	/**
	 * Add the * package to the tree. It is a package that contains only the
	 * direct classes only and is child of the parent package. It does not exist
	 * in the package explorer.
	 * 
	 * @param arrPackage
	 *            parent package of the special package (.*)
	 * @return special package
	 */
	private ArrPackage addSpecialArrPackage(ArrPackage arrPackage) {
		String name = arrPackage.getName() + ".*";
		ArrPackage specialPackage = getPackageByName(name);
		if (specialPackage == null) {
			specialPackage = new ArrPackage();
			specialPackage.setName(name);
			specialPackage.setParent(arrPackage);
			specialPackage.setProject(this);
		} else {
			System.out.println("Special package already added:\t" + specialPackage.getName());
		}
		return specialPackage;
	}

	private ArrPackage getPackageByName(String name) {
		for (ArrPackage pkg : getPackages()) {
			if (pkg.getName().equals(name)) {
				return pkg;
			}
		}
		return null;
	}

	public ArrayList<IPackageFragment> getIPackages() {
		ArrayList<IPackageFragment> iPackages = new ArrayList<IPackageFragment>();
		for (ArrPackage arrPackage : this.getPackages()) {
			iPackages.add(arrPackage.getIPackage());
		}
		return iPackages;
	}

	public ArrPackage getRootPackage() {
		return rootPackage;
	}

	public void setRootPackage(ArrPackage rootPackage) {
		this.rootPackage = rootPackage;
	}

	public String getName() {
		return this.getProject().getElementName();
	}

	public IJavaProject getProject() {
		return iProject;
	}

	public void setjProject(IJavaProject jProject) {
		this.iProject = jProject;
	}

	/**
	 * Get all packages of the project
	 * 
	 * @return arraylist with all packages (including subpackages and * package)
	 */
	public ArrayList<ArrPackage> getPackages() {
		Set<ArrPackage> set = new HashSet<>();
		set.add(rootPackage);
		set.addAll(rootPackage.getAllSubPackages());
		ArrayList<ArrPackage> packages = new ArrayList<ArrPackage>();
		packages.addAll(set);
		return packages;
	}

	public ArrayList<ArrClass> getClasses() {
		return rootPackage.getClasses();
	}

	public void printPackageTree() {
		rootPackage.printTree();
	}

	public ArrClass getClassByIClass(ICompilationUnit iClass) {
		for (ArrClass clss : getClasses()) {
			if (clss.getiClass().equals(iClass)) {
				return clss;
			}
		}
		return null;
	}

	public ArrClass getClassByName(String className) {
		for (ArrClass currentClss : getClasses()) {
			if (currentClss.getFullName().equals(className + "")) {
				return currentClss;
			}
		}
		return null;
	}

	public String getBinPath() throws JavaModelException {
		String binPath = "";
		if (iProject != null) {
			int pathSize = iProject.getPath().toString().length();
			String outputLocation = iProject.getOutputLocation().toString();
			String binAux = outputLocation.substring(pathSize);
			String location = iProject.getProject().getLocation().toString();
			binPath = location + binAux + File.separator;
		}
		return binPath;
	}

	public String getSrcPath() {
		String srcPath = "";
		if (iProject != null) {
			ArrClass clss = this.getClasses().get(0);
			ArrPackage pkg = clss.getPackage();
			int srcSize = pkg.getIPackage().getElementName().length() + 1;
			srcSize += clss.getiClass().getElementName().length();
			String className = clss.getiClass().getPath().toString();
			String location = iProject.getProject().getLocation().toString();
			int subStrBegin = iProject.getPath().toString().length();
			int subStrSize = className.length() - srcSize;
			srcPath = location + className.substring(subStrBegin, subStrSize);
		}
		return srcPath;
	}

}