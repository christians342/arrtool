package arr.model;

import java.util.ArrayList;
import java.util.HashSet;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.JavaModelException;

import jdepend.framework.JavaClass;

public class ArrClass {

	private ArrPackage arrPackage;
	private JavaClass jClass;
	private ICompilationUnit iClass;
	private HashSet<ArrClass> imports;
	private HashSet<ArrClass> importedBy;

	public ArrClass(ICompilationUnit iClass, ArrPackage arrPackage) {
		this.iClass = iClass;
		this.arrPackage = arrPackage;
		this.imports = new HashSet<ArrClass>();
		this.importedBy = new HashSet<ArrClass>();
	}

	public ArrPackage getPackage() {
		return arrPackage;
	}

	public void setPackage(ArrPackage pkg) {
		this.arrPackage = pkg;
	}

	public JavaClass getjClass() {
		return jClass;
	}

	public void setjClass(JavaClass jClass) {
		this.jClass = jClass;
	}

	public ICompilationUnit getiClass() {
		return iClass;
	}

	public void setiClass(ICompilationUnit iClass) {
		this.iClass = iClass;
	}

	public String getName() {
		return iClass.getElementName();
	}

	// TODO Check if special packages have problems in this way of getting names.
	/**
	 * Return the package name without .java extension. 
	 * If this parent package is a special package, return parent name without *.   
	 * @return
	 */
	public String getFullName() {
		String pkgName = arrPackage.getName();
		if (pkgName.indexOf("*") >= 0) {
			pkgName = arrPackage.getParentName();
		}
		String className = iClass.getElementName();
		int lastDot = className.lastIndexOf(".");
		if (lastDot >= 0) {
			className = className.substring(0, lastDot);
		}
		return pkgName + "." + className;
	}

	// TODO check how to handle dependencies among different projects.
	// TODO try to filter API classes to run faster.
	public void createDependencies(IImportDeclaration[] iImportedClasses) throws JavaModelException {
		ArrPackage pkg = getPackage();
		ArrProject proj = pkg.getProject();
		for (IImportDeclaration iImportedClass : iImportedClasses) {
			String importedClassName = iImportedClass.getElementName();
			ArrClass importedClass = proj.getClassByName(importedClassName);
			if (importedClass != null) {
				this.addImport(importedClass);
				importedClass.addImportedBy(this);
			}
		}
	}

	public void addImportedBy(ArrClass clss) {
		importedBy.add(clss);
	}

	public void removeImportedBy(ArrClass clss) {
		importedBy.add(clss);
	}

	public void addImport(ArrClass clss) {
		imports.add(clss);
	}

	public void removeImported(ArrClass clss) {
		imports.remove(clss);
	}

	public ArrayList<ArrClass> getImports() {
		return new ArrayList<>(imports);
	}

	public ArrayList<ArrClass> getImportedBy() {
		return new ArrayList<>(importedBy);
	}
	
	public void printImports(){
		for (ArrClass imported : getImports()){
			System.out.println(getFullName() + " imports " + imported.getFullName());
		}
		for (ArrClass isImportedBy : getImportedBy()){
			System.out.println(getFullName() + " is imported by " + isImportedBy.getFullName());
		}
	}

}
