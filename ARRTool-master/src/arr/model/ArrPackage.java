package arr.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaModelException;

import jdepend.framework.JavaPackage;

public class ArrPackage {

	private ArrayList<ArrPackage> subPackages;
	private ArrPackage parent;
	private ArrayList<ArrClass> classes;
	private IPackageFragment iPackage;
	private JavaPackage jPackage;
	private ArrProject project;
	private String name;

	public ArrPackage() {
		this.subPackages = new ArrayList<ArrPackage>();
		this.classes = new ArrayList<ArrClass>();
		this.parent = null;
		this.iPackage = null;
	}

	/**
	 * Get all subpackages, which includes all subpackages of its subpackage.
	 * @return ArrayList of packages
	 */
	public ArrayList<ArrPackage> getAllSubPackages() {
		ArrayList<ArrPackage> packages = new ArrayList<ArrPackage>();
		packages.addAll(this.getSubPackages());
		for (ArrPackage subpackage : this.getSubPackages()) {
			packages.addAll(subpackage.getAllSubPackages());
		}
		return packages;
	}

	public ArrPackage(ArrProject project, IPackageFragment pkg) {
		this.iPackage = pkg;
		this.project = project;
		this.subPackages = new ArrayList<ArrPackage>();
		this.classes = new ArrayList<ArrClass>();
	}

	public ArrayList<ArrPackage> getSubPackages() {
		return subPackages;
	}

	public void addSubPackage(ArrPackage subPackage) {
		this.subPackages.add(subPackage);
		subPackage.setParent(this);
	}

	public void removeSubPackage(ArrPackage pkg) {
		this.subPackages.remove(pkg);
		pkg.parent = null;
	}

	public ArrPackage getParent() {
		return parent;
	}

	public void setParent(ArrPackage parent) {
		this.parent = parent;
		parent.subPackages.add(this);
	}

	/**
	 * Provide parent name based on the current package name. Special cases are
	 * first level package, such as "" or "br", that parent name is "root"
	 * parent.
	 * 
	 * @return parent name
	 */
	public String getParentName() {
		String name = getName();
		// root
		if (name == null) {
			return "root";
		} else {
			int lastDot = name.lastIndexOf(".");
			// child of root
			if (lastDot == -1) {
				return "root";
			} else {
				// child of any other package
				return name.substring(0, lastDot);
			}
		}
	}

	/**
	 * Return all direct classes and all classes of subpackages tree
	 * 
	 * @return ArrClasses of this package tree
	 */
	public ArrayList<ArrClass> getClasses() {
		Set<ArrClass> classes = new HashSet<ArrClass>(this.classes);
		for (ArrPackage subPackage : this.getSubPackages()) {
			classes.addAll(subPackage.getClasses());
		}
		ArrayList<ArrClass> auxClasses = new ArrayList<>(classes);
		return auxClasses;
	}

	public void addClass(ArrClass classs) {
		this.classes.add(classs);
		classs.setPackage(this);
	}

	public IPackageFragment getIPackage() {
		if (iPackage == null){
			return parent.iPackage;
		}
		return iPackage;
	}

	public void setiPackage(IPackageFragment iPackage) {
		this.iPackage = iPackage;
	}

	public JavaPackage getjPackage() {
		return jPackage;
	}

	public void setjPackage(JavaPackage jPackage) {
		this.jPackage = jPackage;
	}

	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Return the package name according to the iPackage name if it is set. If
	 * it is not, return the name in the variable this.name.
	 * 
	 * @return
	 */
	public String getName() {
		if (iPackage == null) {
			return name;
		} else {
			return iPackage.getElementName();
		}
	}

	public ArrProject getProject() {
		return project;
	}

	public void setProject(ArrProject project) {
		this.project = project;
	}

	/**
	 * Check if this package is an emulated packages (*) that have children and elements.
	 * @return
	 * @throws JavaModelException
	 */
	public boolean isSpecialPackage() throws JavaModelException {
		boolean subPackages = iPackage.hasSubpackages();
		boolean classFiles = iPackage.getCompilationUnits().length > 0;
		boolean name = iPackage.getElementName() != "";
		return subPackages && classFiles && name;
	}

	/**
	 * Get the hierarchy level of this package based on the package separator "." in this case.
	 * @return int of how many parents this package has.
	 */
	public int getLevel() {
		return (getName().length() - getName().replace(".", "").length());
	}

	public void printTree() {
		for (ArrPackage child : subPackages) {
			String dashes = "";
			for (int i = 0; i < child.getLevel(); i++) {
				dashes += "-";
			}
			String line = dashes + child.getName() + "[" + child.getSubPackages().size() + "]" + "("
					+ child.getClasses().size() + ")" + "{" + child.getImportPackages().size() + "}{"
					+ child.getImportedByPackages().size() + "}";
			System.out.println(line);
			String imports = dashes + " -> ";
			for (ArrPackage pkg : child.getImportPackages()) {
				System.out.println(imports + pkg.getName());
			}
			String imported = dashes + " <- ";
			for (ArrPackage pkg : child.getImportedByPackages()) {
				System.out.println(imported + pkg.getName());
			}

			child.printTree();
		}
	}

	public void addClasses(ICompilationUnit[] iClasses) {
		for (ICompilationUnit newIClass : iClasses) {
			ArrClass newClass = new ArrClass(newIClass, this);
			this.classes.add(newClass);
		}

	}

	/**
	 * Create all arrClass dependencies and consequently arrpackage dependencies
	 * because arrpackage dependencies are based on arrclass. Only arrclasses
	 * have explicity dependencies. ArrPackages have imports and importedBy
	 * based on their arrclasses.
	 * 
	 * @throws JavaModelException
	 */
	public void createDependencies() throws JavaModelException {
		ArrayList<ArrClass> classes = this.getClasses();
		for (ArrClass clss : classes) {
			ICompilationUnit iClass = clss.getiClass();
			IImportDeclaration[] iImportedClasses = iClass.getImports();
			clss.createDependencies(iImportedClasses);
		}
	}

	/**
	 * Get all package that this package imports based on its arrclasses.
	 * 
	 * @return all import in this package tree.
	 */
	public ArrayList<ArrPackage> getImportPackages() {		
		HashSet<ArrPackage> importPackages = new HashSet<ArrPackage>();
		for (ArrClass clss : getClasses()) {
			for (ArrClass importClss : clss.getImports()) {				
				ArrPackage importPackage = importClss.getPackage();
				if (!importPackage.isSubpackageOf(this) && !this.isSubpackageOf(importPackage)){
					importPackages.add(importPackage);
					for (ArrPackage importSubpackage : importPackage.getAllSubPackages()){
						if (!importSubpackage.isSubpackageOf(this) && !this.isSubpackageOf(importSubpackage)){
							importPackages.add(importSubpackage);
						}
					}
					for (ArrPackage importParentPackage : importPackage.getParentPackages()){
						if (!importParentPackage.isSubpackageOf(this) && !this.isSubpackageOf(importParentPackage)){
							importPackages.add(importParentPackage);
						}
					}
				}
			}
		}
		return new ArrayList<ArrPackage>(importPackages);
	}
	
	/**
	 * Get the package that import this package considering this package tree
	 * 
	 * @return all packages that import this package
	 */
	public ArrayList<ArrPackage> getImportedByPackages() {
		HashSet<ArrPackage> importedByPackages = new HashSet<ArrPackage>();
		for (ArrClass clss : getClasses()) {
			for (ArrClass importedByClss : clss.getImportedBy()) {				
				ArrPackage importedByPackage = importedByClss.getPackage();
				if (!importedByPackage.isSubpackageOf(this) && !this.isSubpackageOf(importedByPackage)){
					importedByPackages.add(importedByPackage);
					for (ArrPackage importedBySubpackage : importedByPackage.getAllSubPackages()){
						if (!importedBySubpackage.isSubpackageOf(this) && !this.isSubpackageOf(importedBySubpackage)){
							importedByPackages.add(importedBySubpackage);
						}
					}
					for (ArrPackage importedByParentPackage : importedByPackage.getParentPackages()){
						if (!importedByParentPackage.isSubpackageOf(this) && !this.isSubpackageOf(importedByParentPackage)){
							importedByPackages.add(importedByParentPackage);
						}
					}
				}
			}
		}
		return new ArrayList<ArrPackage>(importedByPackages);
	}

	public boolean isSubpackageOf(ArrPackage parentPackage) {
		return this.getAllSubPackages().contains(parentPackage);
	}
	
	/**
	 * Get all parent packages excepting the root package of current project.
	 * @return ArrayList of parent packages.
	 */
	public ArrayList<ArrPackage> getParentPackages(){
		ArrayList<ArrPackage> parents = new ArrayList<ArrPackage>();
		ArrPackage parentPackage = this.getParent();
		while (parentPackage != this.project.getRootPackage()){
			parents.add(parentPackage);
			parentPackage = parentPackage.getParent();
		}
		return parents;
	}

}