package arr.util;

import java.util.ArrayList;

import org.eclipse.jdt.core.IPackageFragment;

import arr.model.ArrPackage;

public class Util {

	public static IPackageFragment findIPackageByName(String pkgName, IPackageFragment[] packages) {
		for (IPackageFragment currentIPkg : packages) {
			if (currentIPkg.getElementName().equals(pkgName)) {
				return currentIPkg;
			}
		}
		return null;
	}

	public static ArrPackage findArrPackageByIPackage(IPackageFragment iPkg, ArrayList<ArrPackage> arrPkgs) {
		for (ArrPackage currentArrPkg : arrPkgs) {
			if (currentArrPkg.getIPackage().equals(iPkg)) {
				return currentArrPkg;
			}
		}
		return null;
	}
	
	public static ArrPackage findArrPackageByName(String name, ArrayList<ArrPackage> packages) {
		for (ArrPackage currentArrPkg : packages) {
			if (currentArrPkg.getName().equals(name)) {
				return currentArrPkg;
			}
		}
		return null;
	}

}
