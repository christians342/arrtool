package arr.general;

import java.util.ArrayList;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IImportDeclaration;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import jdepend.framework.JavaClass;

/*
 * The matrix will be NxM (line x column) and N is the number of classes (classElements)
 * and M will be the number of packages
 */

public class CodeDependencyMatrix {

	private boolean[][] matrix;
	private ArrayList<ARRJavaPackage> packageElements;
	private ArrayList<JavaClass> classElements;
	private ArrayList<CodeDependency> dependencyList;

	// TODO CodeDependency considering many projects in the same workspace.
	/**
	 * One project to code matrix
	 * 
	 * @param p
	 *            IProject
	 * @throws JavaModelException
	 */
	public CodeDependencyMatrix(IProject p) throws JavaModelException {
		IJavaProject project = JavaCore.create(p);
		IPackageFragment[] packages = project.getPackageFragments();
		for (IPackageFragment currentPackage : packages) {
			ICompilationUnit[] compilationUnits = currentPackage.getCompilationUnits();
			for (ICompilationUnit currentClass : compilationUnits) {
				for (IImportDeclaration importedPackage : currentClass.getImports()) {
					String importedPackageName = importedPackage.getElementName();
					int lastDot = importedPackageName.lastIndexOf(".");
					importedPackageName = importedPackageName.substring(0, lastDot);
					for (IPackageFragment pkg : packages) {
						if (pkg.getElementName().equals(importedPackageName)) {
							System.out.println("\t[C][Imports]" + pkg.getElementName());
						}
					}

				}
			}
		}

	}

	public CodeDependencyMatrix(ArrayList<JavaClass> projectClasses, ArrayList<ARRJavaPackage> projectPackages,
			ArrayList<CodeDependency> dependencyList) {
		matrix = new boolean[projectClasses.size()][projectPackages.size()];
		this.classElements = projectClasses;
		this.packageElements = projectPackages;
		this.dependencyList = dependencyList;
		matrix = new boolean[projectClasses.size()][projectPackages.size()];
		for (int i = 0; i < projectClasses.size(); i++)
			for (int j = 0; j < projectPackages.size(); j++)
				matrix[i][j] = false;
	}

	public void calculateMatrix() {
		System.out.println("\nCalculando Matriz de depend�ncias para o SPMF:");
		System.out.println("N�mero de classes: " + classElements.size() + "\nN�mero de Pacotes: "
				+ packageElements.size() + "\nN�mero de depend�ncias: " + dependencyList.size());

		// Need to compare with == and not with .equals because JDepend
		// framework overwrites the .compare method on jClass and JavaPackage.
		for (int i = 0; i < classElements.size(); i++)
			for (int j = 0; j < packageElements.size(); j++)
				for (int z = 0; z < dependencyList.size(); z++)
					if (classElements.get(i) == (dependencyList.get(z).getjClass())
							&& packageElements.get(j) == (dependencyList.get(z).getjPackage()))
						matrix[i][j] = true;
		// printMatrix();
		calculateNumberOfTransactions();

	}

	public boolean[][] getMatrix() {
		return matrix;
	}

	public void setMatrix(boolean[][] matrix) {
		this.matrix = matrix;
	}

	public ArrayList<ARRJavaPackage> getPackageElements() {
		return packageElements;
	}

	public void setPackageElements(ArrayList<ARRJavaPackage> packageElements) {
		this.packageElements = packageElements;
	}

	public ArrayList<JavaClass> getClassElements() {
		return classElements;
	}

	public void setClassElements(ArrayList<JavaClass> classElements) {
		this.classElements = classElements;
	}

	public ArrayList<CodeDependency> getDependencyList() {
		return dependencyList;
	}

	public void setDependencyList(ArrayList<CodeDependency> dependencyList) {
		this.dependencyList = dependencyList;
	}

	private void calculateNumberOfTransactions() {
		boolean used = false;
		for (int i = 0; i < classElements.size(); i++) {
			for (int j = 0; j < packageElements.size(); j++) {
				if (matrix[i][j] == true) {
					if (used == false) {
						used = true;
					}
				}
			}
			used = false;
		}

	}

	@SuppressWarnings("unused")
	private void printMatrix() {
		for (int i = 0; i < classElements.size(); i++) {
			for (int j = 0; j < packageElements.size(); j++) {
				System.out.print(Boolean.toString(matrix[i][j]) + " ");
			}
			System.out.println("");
		}

	}

}
