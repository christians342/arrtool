package arr.algorithms;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import arr.general.ARRJavaPackage;
import arr.general.CodeDependencyMatrix;
import arr.util.ProjectUtilities;

public class AlgorithmParser {

	File spmfFile;
	CodeDependencyMatrix matrix;

	public AlgorithmParser(File f) {
		spmfFile = f;
		matrix = ProjectUtilities.getDependencyMatrix();
	}

	public ArrayList<AlgorithmOutput> parse() throws FileNotFoundException {
		ArrayList<AlgorithmOutput> outputData = new ArrayList<AlgorithmOutput>();
		Scanner input = new Scanner(spmfFile);
		int totalNumberOfTransactions = 0;

		int numberOfTargetPackages;
		int numberOfBasePackages;

		// cada linha do arquivo eh um suporte maior que o minimo ja
		// cada linha tem as dependencias que sao para ser colocadas na tela
		// 
		while (input.hasNext()) {
			numberOfBasePackages = 0;
			numberOfTargetPackages = 0;

			String nextLine = input.nextLine();

			// cada linha pode ter vários base and target packages
			ArrayList<ARRJavaPackage> targetPackages = new ArrayList<ARRJavaPackage>();
			ArrayList<ARRJavaPackage> basePackages = new ArrayList<ARRJavaPackage>();

			String[] lineTokens = nextLine.split(" #SUP: ");
			int support = Integer.parseInt(lineTokens[1]);
			
			String[] packageTokens = lineTokens[0].split(" ");

			for (String packageToken : packageTokens) {
				int packageInt = Integer.parseInt(packageToken);
				int packagesSize = matrix.getPackageElements().size();
				if ( packageInt <= packagesSize) {
					numberOfTargetPackages++;					
					ARRJavaPackage targetPackage = matrix.getPackageElements().get(packageInt);
					targetPackages.add(targetPackage);
				} else {
					numberOfBasePackages++;
					// para adicionar no jdepend os base package tem os ids deles +10000
					int basePackageInt = packageInt - 10000;
					ARRJavaPackage basePackage = matrix.getPackageElements().get(basePackageInt);
					basePackages.add(basePackage);
				}
			}
			
			if (numberOfBasePackages > 0 && numberOfTargetPackages > 0) {				
				for (int target = 0; target < targetPackages.size(); target++) {
					String targetName = targetPackages.get(target).getName();
					for (int base = 0; base < basePackages.size(); base++) {
						String baseName = basePackages.get(base).getName();
						if (!targetName.contains(baseName) &&
								!baseName.contains(targetName)){
							int numBaseClasses = 0;
							int numTargetClasses = 0;
							for (jdepend.framework.JavaClass clss : matrix.getClassElements()){
								String clssName = clss.getName();
								if (clssName.contains(baseName)){
									numBaseClasses++;
								}
								if (clssName.contains(targetName)){
									numTargetClasses++;
								}
							}
							double newNumBaseClasses = numBaseClasses * 1.0;
							double newSupport = (double) support / newNumBaseClasses;							
							
							ArrayList<ARRJavaPackage> targetAux = new ArrayList<ARRJavaPackage>();
							ArrayList<ARRJavaPackage> baseAux = new ArrayList<ARRJavaPackage>();							
							targetAux.add(targetPackages.get(target));
							baseAux.add(basePackages.get(base));
							// if a dependency was already added
							// keep the higher support
							boolean newDep = true;
							for (AlgorithmOutput dep : outputData){
								if (dep.basePackages.get(0).getName().equals(baseAux.get(0).getName()) &&
										dep.usedPackages.get(0).getName().equals(targetAux.get(0).getName())){
									if ( dep.suport < newSupport){
										//System.out.println("Replaced:"+baseName+"\t"+numBaseClasses+"\t"+targetName+"\t"+numTargetClasses+"\t"+String.valueOf(dep.suport));
										dep.suport = newSupport;										
										//System.out.println("New:"+baseName+"\t"+numBaseClasses+"\t"+targetName+"\t"+numTargetClasses+"\t"+String.valueOf(newSupport));
									} else {
										newDep = false;
									}
									break;
								}
							}						
							if (newDep){
								outputData.add(new AlgorithmOutput(baseAux, targetAux, newSupport));
								totalNumberOfTransactions++;
								//System.out.println(baseName+"\t"+numBaseClasses+"\t"+targetName+"\t"+numTargetClasses+"\t"+String.valueOf(newSupport));
							}
						}
					}
				}
			}
		}

		input.close();
		System.out.println("Total number of Transactions: " + totalNumberOfTransactions);
		return outputData;
	}}
