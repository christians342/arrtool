package arr.graphiti.features;

import org.eclipse.graphiti.examples.tutorial.StyleUtil;
import org.eclipse.graphiti.features.IFeatureProvider;
import org.eclipse.graphiti.features.context.IAddContext;
import org.eclipse.graphiti.features.impl.AbstractAddShapeFeature;
import org.eclipse.graphiti.mm.algorithms.Ellipse;
import org.eclipse.graphiti.mm.algorithms.Polyline;
import org.eclipse.graphiti.mm.algorithms.Rectangle;
import org.eclipse.graphiti.mm.algorithms.RoundedRectangle;
import org.eclipse.graphiti.mm.algorithms.Text;
import org.eclipse.graphiti.mm.algorithms.styles.LineStyle;
import org.eclipse.graphiti.mm.algorithms.styles.Orientation;
import org.eclipse.graphiti.mm.algorithms.styles.Style;
import org.eclipse.graphiti.mm.pictograms.BoxRelativeAnchor;
import org.eclipse.graphiti.mm.pictograms.ContainerShape;
import org.eclipse.graphiti.mm.pictograms.Diagram;
import org.eclipse.graphiti.mm.pictograms.PictogramElement;
import org.eclipse.graphiti.mm.pictograms.Shape;
import org.eclipse.graphiti.services.Graphiti;
import org.eclipse.graphiti.services.IGaService;
import org.eclipse.graphiti.services.IPeCreateService;

import arr.general.ARRJavaPackage;

public class AddJavaPackageFeature extends AbstractAddShapeFeature {

	// the additional size of the invisible rectangle at the right border
	// (this also equals the half width of the anchor to paint there)
	public static final int INVISIBLE_RECT_RIGHT = 6;
	private static final int MIN_HEIGHT = 50;
	private static final int MIN_WIDTH = 100;

	public AddJavaPackageFeature(IFeatureProvider fp) {
		super(fp);
	}

	public boolean canAdd(IAddContext context) {
		// check if user wants to add a ARRJavaPackage
		final Object newObject = context.getNewObject();
		if (newObject instanceof ARRJavaPackage) {
			// check if user wants to add to a diagram
			if (context.getTargetContainer() instanceof Diagram) {
				return true;
			}
		}
		return false;
	}

	public PictogramElement add(IAddContext context) {
		ARRJavaPackage addedPackage = (ARRJavaPackage) context.getNewObject();
        Diagram targetDiagram = (Diagram) context.getTargetContainer();
               
        // CONTAINER SHAPE WITH ROUNDED RECTANGLE
 		final IPeCreateService peCreateService = Graphiti.getPeCreateService();
 		final ContainerShape containerShape = peCreateService.createContainerShape(targetDiagram, true);
 		
 		// check whether the context has a size (e.g. from a create feature)
 		// otherwise define a default size for the shape
 		final int width = context.getWidth() <= 0 ? MIN_WIDTH : context.getWidth();
 		final int height = context.getHeight() <= 0 ? MIN_HEIGHT : context.getHeight();

 		final IGaService gaService = Graphiti.getGaService();
 		RoundedRectangle roundedRectangle; // need to access it later
 		{
 			// create invisible outer rectangle expanded by
 			// the width needed for the anchor
 			roundedRectangle = gaService.createPlainRoundedRectangle(containerShape, 5,5);
 			roundedRectangle.setStyle(StyleUtil.getStyleForEClass(targetDiagram));
 			gaService.setLocationAndSize(roundedRectangle, context.getX(), context.getY(), width + INVISIBLE_RECT_RIGHT, height);

 			if(addedPackage.isSpecialPackage())
 				roundedRectangle.setLineStyle(LineStyle.DASH);	
 
 			// if addedPackage has no resource we add it to the resource of the diagram
 			// in a real scenario the business model would have its own resource
 			if (addedPackage.eResource() == null) {
 				getDiagram().eResource().getContents().add(addedPackage);
			}
 			
 			// create link and wire it
 			link(containerShape, addedPackage);
 		}
 		

		// SHAPE WITH TEXT
		{
			// create shape for text
			Shape shape = peCreateService.createShape(containerShape, false);

			// create and set text graphics algorithm
			Text text = gaService.createText(shape, addedPackage.getName());
			
			text.setHorizontalAlignment(Orientation.ALIGNMENT_CENTER);
			gaService.setLocationAndSize(text, 0, 0, width, 20);

			// create link and wire it
			link(shape, addedPackage);
		}
		
		// SHAPE WITH LINE
		{
			// create shape for line
			Shape shape = peCreateService.createShape(containerShape, false);

			// create and set graphics algorithm
			Polyline polyline = gaService.createPlainPolyline(shape, new int[] { 0, 20, width, 20 });
			polyline.setStyle(StyleUtil.getStyleForEClass(getDiagram()));
		}

		// add a chopbox anchor to the shape (if it doesn't exist everything breaks)
		peCreateService.createChopboxAnchor(containerShape);

		// create an additional box relative anchor at middle-right
		final BoxRelativeAnchor boxAnchor = peCreateService.createBoxRelativeAnchor(containerShape);
		boxAnchor.setRelativeWidth(1.0);
		boxAnchor.setRelativeHeight(0.38); // Use golden section

		// anchor references visible rectangle instead of invisible rectangle
		boxAnchor.setReferencedGraphicsAlgorithm(roundedRectangle);

		// assign a graphics algorithm for the box relative anchor
		final Ellipse ellipse = gaService.createPlainEllipse(boxAnchor);

		// anchor is located on the right border of the visible rectangle
		// and touches the border of the invisible rectangle
		final int w = INVISIBLE_RECT_RIGHT;
		gaService.setLocationAndSize(ellipse, -w, -w, 2 * w, 2 * w);
		ellipse.setStyle(StyleUtil.getStyleForEClass(targetDiagram));


 		// call the layout feature
 		layoutPictogramElement(containerShape);
 		return containerShape;

    }
	
	public static Style getStyleForCommonValues(Diagram diagram) {
        final String styleId = "COMMON-VALUES";
        IGaService gaService = Graphiti.getGaService();

        // Is style already persisted?
        Style style = gaService.findStyle(diagram, styleId);

        if (style == null) { // style not found - create new style
            style = gaService.createPlainStyle(diagram, styleId);
            setCommonValues(style);
        }
        return style;
    }
	
    public static Style getStyleForPackageDescription(Diagram diagram) {
        final String styleId = "PACKAGE-TEXT";
        IGaService gaService = Graphiti.getGaService();

        // this is a child style of the common-values-style
        Style parentStyle = getStyleForCommonValues(diagram);
        Style style = gaService.createPlainStyle(parentStyle, styleId);
        
        style.setFilled(false);
        style.setFont(gaService.manageDefaultFont(diagram, false, false));
        style.setHorizontalAlignment(Orientation.ALIGNMENT_LEFT);
        style.setVerticalAlignment(Orientation.ALIGNMENT_TOP);
        
        return style;
    }
    
    private static void setCommonValues(Style style) {
        style.setLineStyle(LineStyle.SOLID);
        style.setLineVisible(true);
        style.setLineWidth(2);
        style.setTransparency(0.0);
    }
}

