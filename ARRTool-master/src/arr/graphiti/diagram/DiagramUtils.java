package arr.graphiti.diagram;

public class DiagramUtils {

	
	public DiagramUtils(){
		
	}
	
	public int getX(int total, int currentPackage){
		int radius = total * 50;
		int centerX = radius/2;
		double slice = 2 * 3.1415 / total;
		double angle = slice * currentPackage;
		return (int)(centerX + radius * Math.cos(angle));
	}
	
	public int getY(int total, int currentPackage){
		int radius = total * 25;
		int centerY = radius/2;
		double slice = 2 * 3.1415 / total;
		double angle = slice * currentPackage;
		return (int)(centerY + radius * Math.sin(angle));
	}
}
